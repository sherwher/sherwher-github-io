---
layout: post
title: "ReactWithSpringBoot-리엑트, 스프링부트 연동"
date: 2018-12-26
excerpt: ""
tags: [post, react, springboot, proxy]
comments: false
---

# React와 SpringBoot연동(Frontend: React, Backend: SpringBoot, Deploy: jar)

### Window기준으로 작성되었습니다.

##### window기준으로 mvn이 먹히지 않을경우 .\mvnw로 변경하여 돌리시면 됩니다. 또한 소스코드는 이클립스를 사용하셔도 되지만 Visual Studio Code에 React관련 extension과 JavaExtension Pack, Spring ExtensionPacK을 사용하여 개발하였습니다.

#### node.js가 설치되어 있어야합니다.

## 1. SpringBoot Project설정
<https://start.spring.io> 사이트에서 Spring Boot설정.

dependency에 web을 추가.

## 2. pom.xml설정

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>no.kantega</groupId>
    <artifactId>spring-and-react</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <packaging>jar</packaging>

    <name>spring-and-react</name>
    <description>Demo project for Spring Boot</description>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.0.1.RELEASE</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <java.version>1.8</java.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>
</project>
```

## 3. Controller추가
처음 설정된 프로젝트는 src하위로 아무것도 존재하지 않으므로 Controller를 추가해줍니다.

참고로 vscode에서 사용시 auto import는 alt + shift + o입니다.

```java
package no.kantega.springandreact;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class HelloController {
    @GetMapping("/api/hello")
    public String hello() {
        return "Hello, the time at the server is now " + new Date() + "\n";
    }
}
```

## 4. 실행
일단 Spring은 maven기반이므로 한번 돌려서 필요한 라이브러리들을 끌어오고 실행이 되는지 확인을 합니다.

## 5. React 추가
터미널을 실행하여 해당 프로젝트 폴더에서 다음 명령어를 실행합니다.
```
npx create-react-app frontend
```

## 6. http-proxy-middleware를 추가
현재 추가된 React는 port 3000번에서 실행되며 SpringBoot는 port 8080에서 실행되게 됩니다. 개발시 port 3000에서 8080으로 넘겨주는 역활을 할 예정입니다.

아까 그 터미널 창에서 다음 명령어를 사용합니다.
```
cd frontend
npm install --save http-proxy-middleware
```

## 7. React 실행
실행위치는 frontend 폴더 안이어야합니다. 
위과정을 거쳤을 경우 이미 frontend안이라 딱히 상관없습니다.
```
npm start
```

초기 실행시에는 시간이 걸릴 수 있습니다.

## 8. proxy연결
fontend/src/setupProxy.js추가

setupProxy.js
```javascript
const proxy = require('http-proxy-middleware')

module.exports = function(app) {
    app.use(proxy('/', { target: 'http://localhost:8080/' }));
}

```
그 후 React 재실행(아까 실행했던 터미널에서 ctrl + c로 종료후 다시 실행)
```
npm start
```

연결됬는지 확인은 
```
curl http://localhost:3000/api/hello
```
터미널에 입력 결과가 나오면 잘된 것.

그후 frontend/src/App.js파일을 열어 다음과 같이 수정

App.js
```javascript
import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {

    state = {};

    componentDidMount() {
        setInterval(this.hello, 250);
    }

    hello = () => {
        fetch('/api/hello')
            .then(response => response.text())
            .then(message => {
                this.setState({message: message});
            });
    };

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h1 className="App-title">{this.state.message}</h1>
                </header>
                <p className="App-intro">
                    To get started, edit <code>src/App.js</code> and save to reload.
                </p>
            </div>
        );
    }
}

export default App;
```
화면이 제대로 바뀌는지 확인

## 9. 배포용 패키지 생성
pom.xml안에 /build/plugins에 기존 plugin아래 내용 추가
```xml
<plugin>
    <groupId>com.github.eirslett</groupId>
    <artifactId>frontend-maven-plugin</artifactId>
    <version>1.6</version>
    <configuration>
        <workingDirectory>frontend</workingDirectory>
        <installDirectory>target</installDirectory>
    </configuration>
    <executions>
        <execution>
            <id>install node and npm</id>
            <goals>
                <goal>install-node-and-npm</goal>
            </goals>
            <configuration>
                <nodeVersion>v8.9.4</nodeVersion>
                <npmVersion>5.6.0</npmVersion>
            </configuration>
        </execution>
        <execution>
            <id>npm install</id>
            <goals>
                <goal>npm</goal>
            </goals>
            <configuration>
                <arguments>install</arguments>
            </configuration>
        </execution>
        <execution>
            <id>npm run build</id>
            <goals>
                <goal>npm</goal>
            </goals>
            <configuration>
                <arguments>run build</arguments>
            </configuration>
        </execution>
    </executions>
</plugin>
```
추가후

기존 mvn spring-boot:run 했던 터미널창에서 ctrl + C 연타하여 서버 다운후 재가동.
```
mvn clean install
```
위 과정은 react파일을 spring boot에 포함시키기기 위해 빌드했습니다.

## 10. 빌드된 파일 jar파일로 만들기
pom.xml안에 /build/plugins에 기존 plugin아래 아래내용 추가
```xml
<plugin>
    <artifactId>maven-antrun-plugin</artifactId>
    <executions>
        <execution>
            <phase>generate-resources</phase>
            <configuration>
                <target>
                    <copy todir="${project.build.directory}/classes/public">
                        <fileset dir="${project.basedir}/frontend/build"/>
                    </copy>
                </target>
            </configuration>
            <goals>
                <goal>run</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```
추가후

기존 mvn spring-boot:run 했던 터미널창에서 ctrl + C 연타하여 서버 다운후 재가동.
```
mvn clean install
```
위 과정을 걸치면 projectDir/target/ jar파일이 생성됨.

jar파일을 실행해봅시다.
```
cd target/

java -jar .\reactwithspringboot-0.0.1-SNAPSHOT.jar
```

이렇게 되면 끝

확인은 웹 브라우저에서
```
localhost:8080
```
위 주소로 확인.


# 인용
> <https://start.spring.io/>

> <https://start.goodtime.co.kr/2018/09/%EC%8A%A4%ED%94%84%EB%A7%81-%EB%B6%80%ED%8A%B8-%EB%A6%AC%EC%95%A1%ED%8A%B8-%EA%B0%9C%EB%B0%9C-%EC%85%8B%EC%97%85-2018/>

> <https://github.com/kantega/react-and-spring>
