---
layout: page
title: About the Lee SungKeun
tags: [about, React, React-Native, SpringBoot, Android, iOS, SWIFT]
date: 2018-12-24
comments: false
---
    
<center><a href="https://github.com/sherwher"><b>Lee SungKeun</b></a> is Developer in Korea</center>

## About
프로젝트 특성상 회사 내부 프로젝트가 많아 표현하기 GIT프로젝트가 적은 대한민국의 평범한 개발자

## Site
* GitHub: <a href="https://github.com/sherwher"><b>https://github.com/sherwher</b></a>
* GitLab: <a href="https://gitlab.com/sherwher"><b>https://gitlab.com/sherwher</b></a>
* GitLab: <a href="https://gitlab.com/LSKun"><b>https://gitlab.com/LSKun</b></a>
* stackoverflow: <a href="https://stackoverflow.com/users/10827631/sherwher"><b>https://stackoverflow.com/users/10827631/sherwher</b></a>

## Features
* React-Native: <a href="https://github.com/LSK-ReactNative"><b>LSK-ReactNative</b></a>
* SWIFT: <a href="https://github.com/LSK-SWIFT"><b>LSK-SWIFT</b></a>
* SpringFramework: <a href="https://github.com/LSK-SpringFramework"><b>LSK-SpringFramework</b></a>
* eGovFrame: <a href="https://github.com/LSK-eGovFrame"><b>LSK-eGovFrame</b></a>
* React: <a href="https://github.com/LSK-React"><b>LSK-React</b></a>
* JAVA: <a href="https://github.com/LSK-JAVA"><b>LSK-JAVA</b></a>

